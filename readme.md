Playing around with WebGL, following MDN's [Getting Started with WebGL tutorial](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL)

Live demo: [https://ppx17.gitlab.io/webgl-3d](https://ppx17.gitlab.io/webgl-3d)
